﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Weather_monitor.Models;

namespace Weather_monitor.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetdataXML()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("text/xml"));
                string url = "https://data.tmd.go.th/api/Weather3Hours/V2/?uid=u62sanchai.wj&ukey=7f917133be325d6ae6504d90494476ec";
                var response = await client.GetAsync(url);
                string jsonResult = await response.Content.ReadAsStringAsync();
                if (jsonResult != "")
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(jsonResult);
                    string json = JsonConvert.SerializeXmlNode(doc);
                    JObject jObj = JObject.Parse(json);
                    return Json(jObj);

                }
                return (null);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                return (null);
            }
           
        }
        public async Task<JsonResult> GetdataDust()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string url = "http://air4thai.pcd.go.th/services/getNewAQI_JSON.php?stationID=32t";
                var response = await client.GetAsync(url);
                string jsonResult = await response.Content.ReadAsStringAsync();
                if (jsonResult != "")
                {
                    JObject jObj = JObject.Parse(jsonResult);
                    return Json(jObj);
                }
                return (null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                return (null);
            }

        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
